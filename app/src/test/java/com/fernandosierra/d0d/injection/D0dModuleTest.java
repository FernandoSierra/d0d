package com.fernandosierra.d0d.injection;

import android.content.Context;

import com.fernandosierra.d0d.data.network.D0dApi;

import rx.Scheduler;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.mock;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */
public class D0dModuleTest extends D0dModule {

    @Override
    Context providesContext() {
        return mock(Context.class);
    }

    @Override
    D0dApi providesD0dApi() {
        return mock(D0dApi.class);
    }

    @Override
    Scheduler provideSchedulerBackground() {
        return Schedulers.immediate();
    }

    @Override
    Scheduler provideSchedulerMain() {
        return Schedulers.immediate();
    }
}
