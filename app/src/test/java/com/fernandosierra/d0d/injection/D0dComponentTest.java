package com.fernandosierra.d0d.injection;

import com.fernandosierra.d0d.presentation.BorrowPresenterTest;
import com.fernandosierra.d0d.presentation.DevicesPresenterTest;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */
@Singleton
@Component(modules = {D0dModule.class})
public interface D0dComponentTest extends D0dComponent {
    void inject(DevicesPresenterTest presenterTest);

    void inject(BorrowPresenterTest presenterTest);
}
