package com.fernandosierra.d0d;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import com.fernandosierra.d0d.injection.D0dComponentTest;
import com.fernandosierra.d0d.injection.D0dModuleTest;
import com.fernandosierra.d0d.injection.DaggerD0dComponentTest;

import org.junit.Before;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */

public abstract class BaseTest {

    @CallSuper
    @Before
    public void setUp() throws Exception {
        D0dComponentTest componentTest = DaggerD0dComponentTest.builder()
                .d0dModule(new D0dModuleTest())
                .build();
        injectDependencies(componentTest);
    }

    protected abstract void injectDependencies(@NonNull D0dComponentTest componentTest);
}
