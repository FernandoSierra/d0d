package com.fernandosierra.d0d;

import com.fernandosierra.d0d.presentation.BorrowPresenterTest;
import com.fernandosierra.d0d.presentation.DevicesPresenterTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({DevicesPresenterTest.class, BorrowPresenterTest.class})
public class UnitTestSuite {
}
