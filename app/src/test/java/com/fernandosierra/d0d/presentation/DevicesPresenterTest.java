package com.fernandosierra.d0d.presentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.injection.D0dComponentTest;
import com.fernandosierra.d0d.view.viewinterface.DevicesViewInterface;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/11/16
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DevicesPresenterTest extends BasePresenterTest<DevicesPresenter> {
    private static final int TEST_DEVICES = 3;
    private static final String HOLDER = "Fernando Sierra";
    private static final String NAME = "iPhone_6";
    private static final String ID = "1a2s3d4f5g";
    private static final String NFC = "iphone6_1";
    private List<Device> mDummyList;
    @Mock
    private DevicesViewInterface mMockViewInterface;
    private ArgumentCaptor<Device> mArgumentCaptor;
    private Device mDummyDevice;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        mPresenter.setViewInterface(mMockViewInterface);
        mArgumentCaptor = ArgumentCaptor.forClass(Device.class);
    }

    @Override
    protected void injectDependencies(@NonNull D0dComponentTest componentTest) {
        componentTest.inject(this);
    }

    @Test
    public void testUpdateDevicesWithZeroDevices() throws Exception {
        givenAnEmptyListOfDevices();
        whenUpdateDevices();
        thenViewInterfaceShouldCallShowProgress();
        thenViewInterfaceShouldCallUpdateDevices();
        thenViewInterfaceShouldCallHideProgress();
    }

    private void givenAnEmptyListOfDevices() {
        mDummyList = new ArrayList<>();
        when(mPresenter.mApi.getDevices()).thenReturn(Observable.just(mDummyList));
    }

    private void whenUpdateDevices() {
        mPresenter.updateDevices();
    }

    private void thenViewInterfaceShouldCallShowProgress() {
        verify(mMockViewInterface, times(1)).showProgress();
    }

    private void thenViewInterfaceShouldCallUpdateDevices() {
        verify(mMockViewInterface, times(1)).updateDevices(mDummyList);
    }

    private void thenViewInterfaceShouldCallHideProgress() {
        verify(mMockViewInterface, times(1)).hideProgress();
    }

    @Test
    public void testUpdateDevicesWithDevices() throws Exception {
        givenAListOfDevices();
        whenUpdateDevices();
        thenViewInterfaceShouldCallShowProgress();
        thenViewInterfaceShouldCallUpdateDevices();
        thenViewInterfaceShouldCallHideProgress();
    }

    private void givenAListOfDevices() {
        mDummyList = new ArrayList<>();
        for (int counter = 0; counter < TEST_DEVICES; counter++) {
            mDummyList.add(mock(Device.class));
        }
        when(mPresenter.mApi.getDevices()).thenReturn(Observable.just(mDummyList));
    }

    @Test
    public void testUpdateDevicesAndFails() throws Exception {
        givenANullListOfDevices();
        whenUpdateDevices();
        thenViewInterfaceShouldCallShowProgress();
        thenViewInterfaceShouldCallHideProgress();
        thenViewInterfaceShouldCallShowError();
    }

    private void givenANullListOfDevices() {
        mDummyList = null;
        when(mPresenter.mApi.getDevices()).thenReturn(Observable.just(mDummyList));
    }

    private void thenViewInterfaceShouldCallShowError() {
        verify(mMockViewInterface, times(1)).showError();
    }

    @Test
    public void testGetDeviceByNfcWithNfc() throws Exception {
        givenAValidNfc();
        whenGetDeviceByNfc(NFC);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallNavigateToBorrow();
    }

    private void thenViewInterfaceShouldCallShowProgressDialog() {
        verify(mMockViewInterface, times(1)).showProgressDialog();
    }

    private void thenViewInterfaceShouldCallNavigateToBorrow() {
        verify(mMockViewInterface, times(1)).navigateToBorrow(mArgumentCaptor.capture());
        Device device = mArgumentCaptor.getValue();
        assertNotNull(device);
        assertEquals(mDummyDevice.getId(), device.getId());
        assertEquals(mDummyDevice.getName(), device.getName());
        assertEquals(mDummyDevice.getNfc(), device.getNfc());
        assertEquals(mDummyDevice.getHolder(), device.getHolder());
    }

    private void thenViewInterfaceShouldCallHideProgressDialog() {
        verify(mMockViewInterface, times(1)).hideProgressDialog();
    }

    private void whenGetDeviceByNfc(@Nullable String nfc) {
        mPresenter.getDeviceByNfc(nfc);
    }

    private void givenAValidNfc() {
        createDummyDevice();
        when(mPresenter.mApi.getDeviceByNfc(NFC)).thenReturn(Observable.just(mDummyDevice));
    }

    private void createDummyDevice() {
        mDummyDevice = new Device();
        mDummyDevice.setHolder(HOLDER);
        mDummyDevice.setName(NAME);
        mDummyDevice.setId(ID);
        mDummyDevice.setNfc(NFC);
    }

    @Test
    public void testGetDeviceByNfcAndFails() throws Exception {
        givenAInvalidNfc();
        whenGetDeviceByNfc(null);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallShowError();
    }

    private void givenAInvalidNfc() {
        when(mPresenter.mApi.getDeviceByNfc(null)).thenReturn(
                Observable.error(new RuntimeException()));
    }
}