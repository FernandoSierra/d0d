package com.fernandosierra.d0d.presentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.injection.D0dComponentTest;
import com.fernandosierra.d0d.view.viewinterface.BorrowViewInterface;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import retrofit2.Response;
import rx.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BorrowPresenterTest extends BasePresenterTest<BorrowPresenter> {
    private static final String ID = "1a2s3d4f5g";
    private static final String NAME = "iPhone 6";
    private static final String NFC = "iphone6_1";
    private static final String HOLDER = "Fernando Sierra";
    @Mock
    private BorrowViewInterface mMockViewInterface;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        mPresenter.setViewInterface(mMockViewInterface);
    }

    @Override
    protected void injectDependencies(@NonNull D0dComponentTest componentTest) {
        componentTest.inject(this);
    }

    @Test
    public void testEvaluateAndDrawWithDeviceToBorrow() throws Exception {
        Device device = givenADeviceToBeBorrowed();
        whenEvaluateAndDrawTheDevice(device);
        thenViewInterfaceShouldCallDrawBorrowScenario();
    }

    @NonNull
    private Device givenADeviceToBeBorrowed() {
        Device device = new Device();
        device.setId(ID);
        device.setName(NAME);
        device.setNfc(NFC);
        return device;
    }

    private void whenEvaluateAndDrawTheDevice(@NonNull Device device) {
        mPresenter.evaluateDeviceAndDraw(device);
    }

    private void thenViewInterfaceShouldCallDrawBorrowScenario() {
        verify(mMockViewInterface, times(1)).drawBorrowScenario();
        verifyNoMoreInteractions(mMockViewInterface);
    }

    @Test
    public void testEvaluateAndDrawWithDeviceToReturn() throws Exception {
        Device device = givenADeviceToBeReturned();
        whenEvaluateAndDrawTheDevice(device);
        thenViewInterfaceShouldCallDrawReturnScenario();
    }

    private void thenViewInterfaceShouldCallDrawReturnScenario() {
        verify(mMockViewInterface, times(1)).drawReturnScenario();
        verifyNoMoreInteractions(mMockViewInterface);
    }

    @NonNull
    private Device givenADeviceToBeReturned() {
        Device device = givenADeviceToBeBorrowed();
        device.setHolder(HOLDER);
        return device;
    }

    @Test
    public void testBorrowDeviceWithADeviceWithoutHolder() throws Exception {
        Device device = givenADeviceToBeBorrowed();
        whenBorrowDevice(device);
        thenViewInterfaceShouldCallCloseKeyboard();
        thenViewInterfaceShouldCallShowEmptyHolderError();
    }

    private void whenBorrowDevice(@NonNull Device device) {
        mPresenter.borrowDevice(device);
    }

    private void thenViewInterfaceShouldCallCloseKeyboard() {
        verify(mMockViewInterface, times(1)).closeKeyboard();
    }

    private void thenViewInterfaceShouldCallShowEmptyHolderError() {
        verify(mMockViewInterface, times(1)).showEmptyHolderError();
    }

    @Test
    public void testBorrowDeviceWithADeviceWithHolderAndWasBorrowed() throws Exception {
        Device device = givenADeviceToBeBorrowedWithHolder();
        givenConfigureBorrowResponse(device, true);
        whenBorrowDevice(device);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallBack();
    }

    @NonNull
    private Device givenADeviceToBeBorrowedWithHolder() {
        Device device = givenADeviceToBeBorrowed();
        device.setHolder(HOLDER);
        return device;
    }

    private void thenViewInterfaceShouldCallShowProgressDialog() {
        verify(mMockViewInterface, times(1)).showProgressDialog();
    }

    private void thenViewInterfaceShouldCallHideProgressDialog() {
        verify(mMockViewInterface, times(1)).hideProgressDialog();
    }

    private void thenViewInterfaceShouldCallBack() {
        verify(mMockViewInterface, times(1)).back();
    }

    private void givenConfigureBorrowResponse(@NonNull Device device, boolean willSuccess) {
        givenConfigureResponse(device, HOLDER, willSuccess);
    }

    private void givenConfigureResponse(@NonNull Device device, @Nullable String holder,
                                        boolean willSuccess) {
        mPresenter = spy(mPresenter);
        Device updatedDevice = new Device();
        device.setHolder(holder);
        when(mPresenter.getDeviceToUpdate(anyString())).thenReturn(updatedDevice);
        when(mPresenter.mApi.updateDeviceByNfc(eq(device.getNfc()), any())).thenReturn(
                willSuccess ?
                        Observable.just(Response.success(null)) :
                        Observable.error(new RuntimeException()));
    }

    @Test
    public void testBorrowDeviceWithADeviceWithHolderAndFails() throws Exception {
        Device device = givenADeviceToBeBorrowedWithHolder();
        givenConfigureBorrowResponse(device, false);
        whenBorrowDevice(device);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallShowError();
    }

    private void thenViewInterfaceShouldCallShowError() {
        verify(mMockViewInterface, times(1)).showError();
    }

    @Test
    public void testReturnDeviceAndWasReturned() throws Exception {
        Device device = givenADeviceToBeReturned();
        givenConfigureReturnResponse(device, true);
        whenReturnDevice(device);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallBack();
    }

    private void whenReturnDevice(Device device) {
        mPresenter.returnDevice(device);
    }

    private void givenConfigureReturnResponse(@NonNull Device device, boolean willSuccess) {
        givenConfigureResponse(device, null, willSuccess);
    }

    @Test
    public void testReturnDeviceAndFails() throws Exception {
        Device device = givenADeviceToBeReturned();
        givenConfigureReturnResponse(device, false);
        whenReturnDevice(device);
        thenViewInterfaceShouldCallShowProgressDialog();
        thenViewInterfaceShouldCallHideProgressDialog();
        thenViewInterfaceShouldCallShowError();
    }
}