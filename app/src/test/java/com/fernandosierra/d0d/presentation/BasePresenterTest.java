package com.fernandosierra.d0d.presentation;

import com.fernandosierra.d0d.BaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */
public abstract class BasePresenterTest<T extends BasePresenter> extends BaseTest {
    @Inject
    T mPresenter;

    @Test
    public void testOnStopAndCompositeSubscriptionIsEmpty() throws Exception {
        mPresenter.onStop();
        assertNotNull(mPresenter.mCompositeSubscription);
        assertFalse(mPresenter.mCompositeSubscription.hasSubscriptions());
    }

    @Test
    public void testOnDestroyAndViewInterfaceIsNull() throws Exception {
        mPresenter.onDestroy();
        assertNull(mPresenter.mViewInterface);
    }
}
