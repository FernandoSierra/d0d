package com.fernandosierra.d0d.view.nfc;

import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NfcHelperTest {
    private static final String VALID_BASE_URI = "prisma://borrow/";
    private static final String INVALID_BASE_URI = "http://borrow/";
    private static final String NFC_VALUE = "iphone6_1";
    private static final String VALID_URI = VALID_BASE_URI + NFC_VALUE;
    private static final String INVALID_URI = INVALID_BASE_URI + NFC_VALUE;
    private NfcHelper mNfcHelper;
    private Intent mDummyIntent;
    private String mNfcValue;

    @Before
    public void setUp() throws Exception {
        mNfcHelper = new NfcHelper(InstrumentationRegistry.getTargetContext());
    }

    @Test
    public void testGetNfcValueWithValidIntent() throws Exception {
        givenAValidIntent();
        whenGetNfcValue();
        thenNfcValueIsValid();
    }

    private void thenNfcValueIsValid() {
        assertNotNull(mNfcValue);
        assertEquals(NFC_VALUE, mNfcValue);
    }

    private void whenGetNfcValue() {
        mNfcValue = mNfcHelper.getNfcValue(mDummyIntent);
    }

    private void givenAValidIntent() {
        mDummyIntent = new Intent(NfcAdapter.ACTION_NDEF_DISCOVERED);
        mDummyIntent.setData(Uri.parse(VALID_URI));
    }

    @Test
    public void testGetNfcValueWithInvalidIntent() throws Exception {
        givenAInvalidIntent();
        whenGetNfcValue();
        thenNfcValueIsInvalid();
    }

    private void thenNfcValueIsInvalid() {
        assertNull(mNfcValue);
    }

    private void givenAInvalidIntent() {
        mDummyIntent = new Intent(NfcAdapter.ACTION_NDEF_DISCOVERED);
        mDummyIntent.setData(Uri.parse(INVALID_URI));
    }
}