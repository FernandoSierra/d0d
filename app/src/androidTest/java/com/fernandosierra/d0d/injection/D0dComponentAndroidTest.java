package com.fernandosierra.d0d.injection;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/11/16.
 */
@Singleton
@Component(modules = {D0dModule.class})
public interface D0dComponentAndroidTest extends D0dComponent {
}
