package com.fernandosierra.d0d.injection;

import com.fernandosierra.d0d.BuildConfig;
import com.fernandosierra.d0d.automation.tests.BaseTest;
import com.fernandosierra.d0d.data.network.D0dApi;
import com.jakewharton.espresso.OkHttp3IdlingResource;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/11/16.
 */
public class D0dModuleAndroidTest extends D0dModule {

    private static final String RESOURCE_NAME = "okhttp";

    @Override
    D0dApi providesD0dApi() {
        OkHttpClient client = new OkHttpClient();
        BaseTest.sIdlingResource = OkHttp3IdlingResource.create(RESOURCE_NAME, client);
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.URL_API)
                .client(client)
                .build();
        return retrofit.create(D0dApi.class);
    }
}
