package com.fernandosierra.d0d.automation.tests;

import android.support.test.rule.ActivityTestRule;

import com.fernandosierra.d0d.automation.robots.DevicesRobot;
import com.fernandosierra.d0d.view.DevicesActivity;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DevicesTest extends BaseTest<DevicesActivity> {
    private static final String NFC_VALUE = "iphone6_1";
    private DevicesRobot mRobot;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mRobot = new DevicesRobot(mContext);
    }

    @Override
    public ActivityTestRule<DevicesActivity> getActivityTestRule() {
        return new ActivityTestRule<>(DevicesActivity.class);
    }

    @Test
    public void testFullPath() throws Exception {
        mRobot.refreshList()
                .readFakeNFcDevice(rule.getActivity(), NFC_VALUE)
                .toggleDevice()
                .seeDetail()
                .back();
    }
}
