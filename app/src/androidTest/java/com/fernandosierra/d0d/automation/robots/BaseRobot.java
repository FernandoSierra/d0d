package com.fernandosierra.d0d.automation.robots;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
public class BaseRobot {
    Context mContext;

    public BaseRobot(@NonNull Context context) {
        mContext = context;
    }
}
