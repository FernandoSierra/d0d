package com.fernandosierra.d0d.automation;

import android.support.test.InstrumentationRegistry;

import com.fernandosierra.d0d.D0dApplication;
import com.fernandosierra.d0d.injection.D0dModuleAndroidTest;
import com.fernandosierra.d0d.injection.DaggerD0dComponentAndroidTest;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/11/16.
 */
public class D0dApplicationAndroidTest extends D0dApplication {

    @Override
    public void onCreate() {
        D0dModuleAndroidTest d0dModule = new D0dModuleAndroidTest();
        d0dModule.setContext(InstrumentationRegistry.getTargetContext());
        mComponent = DaggerD0dComponentAndroidTest.builder()
                .d0dModule(d0dModule)
                .build();
    }
}
