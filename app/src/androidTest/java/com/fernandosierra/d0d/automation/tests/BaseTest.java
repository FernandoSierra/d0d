package com.fernandosierra.d0d.automation.tests;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.jakewharton.espresso.OkHttp3IdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public abstract class BaseTest<T extends Activity> {
    public static OkHttp3IdlingResource sIdlingResource;
    @Rule
    public ActivityTestRule<T> rule = getActivityTestRule();
    Context mContext;

    @CallSuper
    @Before
    public void setUp() throws Exception {
        mContext = InstrumentationRegistry.getTargetContext();
        if (sIdlingResource != null) {
            Espresso.registerIdlingResources(sIdlingResource);
        }
    }

    @CallSuper
    @After
    public void tearDown() throws Exception {
        if (sIdlingResource != null) {
            Espresso.unregisterIdlingResources(sIdlingResource);
        }
    }

    public abstract ActivityTestRule<T> getActivityTestRule();
}
