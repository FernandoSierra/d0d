package com.fernandosierra.d0d.automation.robots;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.support.annotation.NonNull;

import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.view.DevicesActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
public class DevicesRobot extends BaseRobot {
    private static final String BASE_URI = "prisma://borrow/";

    public DevicesRobot(@NonNull Context context) {
        super(context);
    }

    public DevicesRobot refreshList() {
        onView(withId(R.id.recycler_devices))
                .perform(swipeDown());
        return this;
    }

    public BorrowRobot readFakeNFcDevice(@NonNull DevicesActivity activity,
                                         @NonNull String nfcValue) {
        Intent intent = new Intent(mContext, activity.getClass());
        intent.setAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        intent.setData(Uri.parse(BASE_URI + nfcValue));
        activity.startActivity(intent);
        return new BorrowRobot(mContext);
    }

    public DevicesRobot seeDetail() {
        onView(withId(R.id.recycler_devices))
                .perform(actionOnItemAtPosition(0, click()));
        return this;
    }

    public DevicesRobot back() {
        pressBack();
        return this;
    }
}
