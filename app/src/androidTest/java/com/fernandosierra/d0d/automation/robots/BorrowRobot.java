package com.fernandosierra.d0d.automation.robots;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.espresso.ViewInteraction;
import android.util.Log;

import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.util.Constants;

import junit.framework.AssertionFailedError;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/11/16.
 */
public class BorrowRobot extends BaseRobot {
    private static final String HOLDER = "Fernando Sierra";

    public BorrowRobot(@NonNull Context context) {
        super(context);
    }

    public DevicesRobot back() {
        closeSoftKeyboard();
        pressBack();
        return new DevicesRobot(mContext);
    }

    public DevicesRobot toggleDevice() {
        ViewInteraction editText = onView(withId(R.id.edit_borrow_holder));
        try {
            editText.check(matches(isEnabled()))
                    .perform(clearText(), typeText(HOLDER));
        } catch (AssertionFailedError e) {
            Log.e(Constants.TAG_LOG, "The device is borrowed");
        }
        onView(withId(R.id.button_borrow_save))
                .perform(click());
        return new DevicesRobot(mContext);
    }
}
