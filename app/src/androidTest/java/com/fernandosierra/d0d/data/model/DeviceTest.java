package com.fernandosierra.d0d.data.model;

import android.os.Parcel;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeviceTest {
    private static final String HOLDER_1 = "Fernando Sierra";
    private static final String HOLDER_2 = "Magally Stransky";
    private static final String NFC_1 = "htc_one_1";
    private static final String NFC_2 = "galaxy_s6";
    private static final String NAME_1 = "HTC One";
    private static final String NAME_2 = "Samsung Galaxy S6";
    private static final String ID = "1a2s3d4f5g";
    private static final int DEVICES_COUNT = 4;
    private Device mDummyDevice;
    private Device mDeviceRead;
    private Parcel mParcel;
    private List<Device> mDevices;

    @Before
    public void setUp() throws Exception {
        mDummyDevice = new Device();
        mParcel = Parcel.obtain();
    }

    @Test
    public void testDeviceParcelable() throws Exception {
        givenAValidDevice();
        whenWriteDeviceToParcel();
        whenReadDeviceFromParcel();
        thenBothDevicesAreEqual();
    }

    private void givenAValidDevice() {
        mDummyDevice.setId(ID);
        mDummyDevice.setName(NAME_1);
        mDummyDevice.setNfc(NFC_1);
        mDummyDevice.setHolder(HOLDER_1);
    }

    private void whenWriteDeviceToParcel() {
        mDummyDevice.writeToParcel(mParcel, 0);
        mParcel.setDataPosition(0);
    }

    private void whenReadDeviceFromParcel() {
        mDeviceRead = Device.CREATOR.createFromParcel(mParcel);
    }

    private void thenBothDevicesAreEqual() {
        assertNotNull(mDeviceRead);
        assertEquals(mDummyDevice, mDeviceRead);
        assertEquals(ID, mDeviceRead.getId());
        assertEquals(NAME_1, mDeviceRead.getName());
        assertEquals(NFC_1, mDeviceRead.getNfc());
        assertEquals(HOLDER_1, mDeviceRead.getHolder());
    }

    @Test
    public void testCompareTo() throws Exception {
        givenAListOfDevices();
        whenOrderDevices();
        thenDevicesAreOrdered();
    }

    private void givenAListOfDevices() {
        mDevices = new ArrayList<>();
        mDevices.add(getFourthDevice());
        mDevices.add(getFirstDevice());
        mDevices.add(getThirdDevice());
        mDevices.add(getSecondDevice());
    }

    private Device getFirstDevice() {
        Device device = new Device();
        device.setId("1");
        device.setName(NAME_1);
        device.setNfc(NFC_1);
        return device;
    }

    private Device getSecondDevice() {
        Device device = new Device();
        device.setId("2");
        device.setName(NAME_2);
        device.setNfc(NFC_2);
        return device;
    }

    private Device getThirdDevice() {
        Device device = new Device();
        device.setId("3");
        device.setName(NAME_1);
        device.setNfc(NFC_1);
        device.setHolder(HOLDER_1);
        return device;
    }

    private Device getFourthDevice() {
        Device device = new Device();
        device.setId("4");
        device.setName(NAME_2);
        device.setNfc(NFC_2);
        device.setHolder(HOLDER_2);
        return device;
    }

    private void whenOrderDevices() {
        Collections.sort(mDevices);
    }

    private void thenDevicesAreOrdered() {
        assertEquals(DEVICES_COUNT, mDevices.size());
        assertEquals(getFirstDevice(), mDevices.get(0));
        assertEquals(getSecondDevice(), mDevices.get(1));
        assertEquals(getThirdDevice(), mDevices.get(2));
        assertEquals(getFourthDevice(), mDevices.get(3));
    }
}