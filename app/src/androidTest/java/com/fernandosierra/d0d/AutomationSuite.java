package com.fernandosierra.d0d;

import com.fernandosierra.d0d.automation.tests.DevicesTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 6/11/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({DevicesTest.class})
public class AutomationSuite {
}
