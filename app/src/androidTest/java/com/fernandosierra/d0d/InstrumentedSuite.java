package com.fernandosierra.d0d;

import com.fernandosierra.d0d.data.model.DeviceTest;
import com.fernandosierra.d0d.view.nfc.NfcHelperTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 4/11/16
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({NfcHelperTest.class, DeviceTest.class})
public class InstrumentedSuite {
}
