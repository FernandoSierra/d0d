package com.fernandosierra.d0d.injection;

import com.fernandosierra.d0d.view.BorrowActivity;
import com.fernandosierra.d0d.view.DevicesActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
@Singleton
@Component(modules = {D0dModule.class})
public interface D0dComponent {
    void inject(DevicesActivity activity);

    void inject(BorrowActivity activity);
}
