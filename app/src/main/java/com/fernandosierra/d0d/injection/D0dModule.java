package com.fernandosierra.d0d.injection;

import android.content.Context;

import com.fernandosierra.d0d.BuildConfig;
import com.fernandosierra.d0d.data.network.D0dApi;
import com.fernandosierra.d0d.presentation.BorrowPresenter;
import com.fernandosierra.d0d.presentation.DevicesPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
@Singleton
@Module
public class D0dModule {
    private Context mContext;

    public void setContext(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    Context providesContext() {
        return mContext;
    }

    @Singleton
    @Provides
    D0dApi providesD0dApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.URL_API)
                .build();
        return retrofit.create(D0dApi.class);
    }

    @Singleton
    @Provides
    DevicesPresenter providesDevicesPresenter(D0dApi api) {
        return new DevicesPresenter(api, provideSchedulerBackground(), provideSchedulerMain());
    }

    Scheduler provideSchedulerBackground() {
        return Schedulers.io();
    }

    Scheduler provideSchedulerMain() {
        return AndroidSchedulers.mainThread();
    }

    @Singleton
    @Provides
    BorrowPresenter providesBorrowPresenter(D0dApi api) {
        return new BorrowPresenter(api, provideSchedulerBackground(), provideSchedulerMain());
    }
}
