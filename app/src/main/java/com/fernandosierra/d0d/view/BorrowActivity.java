package com.fernandosierra.d0d.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fernandosierra.d0d.D0dApplication;
import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.presentation.BorrowPresenter;
import com.fernandosierra.d0d.util.Constants;
import com.fernandosierra.d0d.view.viewinterface.BorrowViewInterface;

import javax.inject.Inject;

public class BorrowActivity extends AppCompatActivity implements BorrowViewInterface {
    @Inject
    protected BorrowPresenter mPresenter;
    private Device mDevice;
    private EditText mHolder;
    private Button mSave;
    private LinearLayout mRoot;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrow);
        init();
    }

    private void init() {
        ((D0dApplication) getApplication()).getComponent().inject(this);
        mPresenter.setViewInterface(this);
        mDevice = getIntent().getParcelableExtra(Constants.Extra.DEVICE);
        setupToolbar();
        initUi();
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.borrow_title);
        }
    }

    private void initUi() {
        TextView name = ((TextView) findViewById(R.id.text_borrow_name));
        mRoot = ((LinearLayout) findViewById(R.id.linear_borrow_container));
        mHolder = ((EditText) findViewById(R.id.edit_borrow_holder));
        mSave = ((Button) findViewById(R.id.button_borrow_save));
        name.setText(mDevice.getName());
        mPresenter.evaluateDeviceAndDraw(mDevice);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @UiThread
    @Override
    public void drawBorrowScenario() {
        mHolder.setEnabled(true);
        mSave.setText(R.string.borrow_borrow);
        mSave.setOnClickListener(view -> {
            mDevice.setHolder(mHolder.getText().toString().trim());
            mPresenter.borrowDevice(mDevice);
        });
    }

    @UiThread
    @Override
    public void drawReturnScenario() {
        mHolder.setText(mDevice.getHolder());
        mHolder.setEnabled(false);
        mSave.setText(R.string.borrow_return);
        mSave.setOnClickListener(view -> mPresenter.returnDevice(mDevice));
    }

    @UiThread
    @Override
    public void showEmptyHolderError() {
        Snackbar.make(mRoot, R.string.borrow_empty_error, Snackbar.LENGTH_LONG).show();
    }

    @UiThread
    @Override
    public void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mHolder.getWindowToken(), 0);
    }

    @UiThread
    @Override
    public void back() {
        onBackPressed();
    }

    @UiThread
    @Override
    public void showError() {
        Snackbar.make(mRoot, R.string.devices_error, Snackbar.LENGTH_LONG).show();
    }

    @UiThread
    @Override
    public void showProgressDialog() {
        mProgressDialog = ProgressDialog.show(this,
                null,
                getString(R.string.borrow_progress_message),
                true,
                false
        );
    }

    @UiThread
    @Override
    public void hideProgressDialog() {
        mProgressDialog.dismiss();
        mProgressDialog = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
