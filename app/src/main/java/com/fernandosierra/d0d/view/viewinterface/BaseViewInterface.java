package com.fernandosierra.d0d.view.viewinterface;

import android.support.annotation.UiThread;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public interface BaseViewInterface {

    /**
     * Displays an error message notifying what happened.
     */
    @UiThread
    void showError();

    /**
     * Builds and shows a modal ProgressDialog.
     */
    @UiThread
    void showProgressDialog();

    /**
     * Disposes and frees the ProgressDialog memory.
     */
    @UiThread
    void hideProgressDialog();
}
