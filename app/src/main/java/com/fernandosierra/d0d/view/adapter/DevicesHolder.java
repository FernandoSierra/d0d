package com.fernandosierra.d0d.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fernandosierra.d0d.R;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
class DevicesHolder extends RecyclerView.ViewHolder {
    View mRoot;
    TextView mDevice;
    TextView mHolder;
    ImageView mImageBorrowed;

    DevicesHolder(View itemView) {
        super(itemView);
        mRoot = itemView;
        mDevice = ((TextView) itemView.findViewById(R.id.text_device_name));
        mHolder = ((TextView) itemView.findViewById(R.id.text_device_holder));
        mImageBorrowed = ((ImageView) itemView.findViewById(R.id.image_device_borrowed));
    }
}
