package com.fernandosierra.d0d.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;

import com.fernandosierra.d0d.D0dApplication;
import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.presentation.DevicesPresenter;
import com.fernandosierra.d0d.util.Constants;
import com.fernandosierra.d0d.view.adapter.DevicesAdapter;
import com.fernandosierra.d0d.view.adapter.DividerItemDecorator;
import com.fernandosierra.d0d.view.nfc.NfcHelper;
import com.fernandosierra.d0d.view.viewinterface.DevicesViewInterface;

import java.util.List;

import javax.inject.Inject;

import static com.fernandosierra.d0d.util.Constants.DIALOG_TAG;

public class DevicesActivity extends AppCompatActivity implements DevicesViewInterface {
    @Inject
    protected DevicesPresenter mPresenter;
    protected NfcHelper mNfcHelper;
    private CoordinatorLayout mCoordinatorLayout;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DevicesAdapter mAdapter;
    private View mEmptyView;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((D0dApplication) getApplication()).getComponent().inject(this);
        mNfcHelper = new NfcHelper(this);
        mPresenter.setViewInterface(this);
        setContentView(R.layout.activity_devices);
        init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    private void init() {
        setupToolbar();
        initUI();
    }

    private void setupToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.devices_title);
        }
    }

    private void initUI() {
        mCoordinatorLayout = ((CoordinatorLayout) findViewById(R.id.coordinator_devices_container));
        mSwipeRefreshLayout = ((SwipeRefreshLayout) findViewById(R.id.swipe_devices));
        RecyclerView recyclerView = ((RecyclerView) findViewById(R.id.recycler_devices));

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.color_accent,
                R.color.color_primary,
                R.color.color_primary_dark);

        mSwipeRefreshLayout.setOnRefreshListener(() -> mPresenter.updateDevices());

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecorator(this));
        mAdapter = new DevicesAdapter();
        mAdapter.setOnDeviceClickListener(device -> {
            DeviceDetailsDialog deviceDetailsDialog = DeviceDetailsDialog.newInstance(device);
            deviceDetailsDialog.show(getSupportFragmentManager(), DIALOG_TAG);
        });
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcHelper.disable(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(DIALOG_TAG);
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.remove(fragment);
            transaction.commit();
        }
        tryToResolveNfcIntent(intent);
    }

    private void tryToResolveNfcIntent(Intent intent) {
        String nfc = mNfcHelper.getNfcValue(intent);
        if (nfc != null) {
            mPresenter.getDeviceByNfc(nfc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.updateDevices();
        mNfcHelper.enable(this);
    }

    @Override
    public void updateDevices(@NonNull List<Device> devices) {
        mAdapter.setDevices(devices);
        mAdapter.notifyDataSetChanged();
        toggleEmptyView(devices.isEmpty());
    }

    private void toggleEmptyView(boolean shouldDisplay) {
        if (shouldDisplay) {
            if (mEmptyView == null) {
                ViewStub stub = ((ViewStub) findViewById(R.id.stub_devices_empty));
                mEmptyView = stub.inflate();
            }
            mEmptyView.setVisibility(View.VISIBLE);
        } else if (mEmptyView != null) {
            mEmptyView.setVisibility(View.GONE);
        }
    }

    @UiThread
    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @UiThread
    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @MainThread
    @Override
    public void navigateToBorrow(@NonNull Device device) {
        Intent intent = new Intent(this, BorrowActivity.class);
        intent.putExtra(Constants.Extra.DEVICE, device);
        startActivity(intent);
    }

    @UiThread
    @Override
    public void showError() {
        toggleEmptyView(mAdapter.getItemCount() == 0);
        Snackbar.make(mCoordinatorLayout, R.string.devices_error, Snackbar.LENGTH_LONG).show();
    }

    @UiThread
    @Override
    public void showProgressDialog() {
        mProgressDialog = ProgressDialog.show(this,
                null,
                getString(R.string.all_nfc_wait),
                true,
                false
        );
    }

    @UiThread
    @Override
    public void hideProgressDialog() {
        mProgressDialog.dismiss();
        mProgressDialog = null;
    }
}
