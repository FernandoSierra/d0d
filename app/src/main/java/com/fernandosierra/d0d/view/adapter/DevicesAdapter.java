package com.fernandosierra.d0d.view.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.data.model.Device;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class DevicesAdapter extends RecyclerView.Adapter<DevicesHolder> {
    private List<Device> mDevices;
    private OnDeviceClickListener mClickListener;

    public DevicesAdapter() {
        mDevices = new ArrayList<>();
    }

    public void setDevices(@NonNull List<Device> devices) {
        mDevices = devices;
    }

    public void setOnDeviceClickListener(@Nullable OnDeviceClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public DevicesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_devices, parent, false);
        return new DevicesHolder(root);
    }

    @Override
    public void onBindViewHolder(DevicesHolder holder, int position) {
        Device device = mDevices.get(position);
        holder.mDevice.setText(device.getName());
        String holderName = device.getHolder();
        holder.mHolder.setText(holderName);
        int visibility;
        boolean isEnabled;
        if (holderName == null) {
            visibility = GONE;
            isEnabled = true;
        } else {
            visibility = VISIBLE;
            isEnabled = false;
        }
        holder.mImageBorrowed.setVisibility(visibility);
        holder.mRoot.setActivated(isEnabled);
        holder.mRoot.setOnClickListener(view -> {
            if (mClickListener != null) {
                mClickListener.onClick(device);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public interface OnDeviceClickListener {
        void onClick(@NonNull Device device);
    }
}
