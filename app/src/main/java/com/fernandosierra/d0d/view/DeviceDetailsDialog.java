package com.fernandosierra.d0d.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.util.Constants;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 5/11/16.
 */
public class DeviceDetailsDialog extends BottomSheetDialogFragment {

    public static DeviceDetailsDialog newInstance(Device device) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.Extra.DEVICE, device);
        DeviceDetailsDialog fragment = new DeviceDetailsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Device device = ((Device) getArguments().get(Constants.Extra.DEVICE));
        View root = inflater.inflate(R.layout.fragment_device_detail, container, false);
        ((TextView) root.findViewById(R.id.text_sheet_name)).setText(device.getName());
        ((TextView) root.findViewById(R.id.text_sheet_id)).setText(device.getId());
        ((TextView) root.findViewById(R.id.text_sheet_nfc)).setText(device.getNfc());
        ((TextView) root.findViewById(R.id.text_sheet_holder)).setText(
                TextUtils.isEmpty(device.getHolder()) ? "-" : device.getHolder());
        return root;
    }
}
