package com.fernandosierra.d0d.view.nfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.d0d.R;
import com.fernandosierra.d0d.util.Constants;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class NfcHelper {
    private final IntentFilter[] mIntentFilters;
    private NfcAdapter mNfcAdapter;
    private String mScheme;
    private String mHost;

    public NfcHelper(@NonNull Context context) {
        mScheme = context.getString(R.string.nfc_scheme);
        mHost = context.getString(R.string.nfc_host);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(context);
        IntentFilter intentFilter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter.addDataScheme(mScheme);
        intentFilter.addDataAuthority(mHost, null);
        mIntentFilters = new IntentFilter[]{intentFilter};
    }

    /**
     * Enables the NFC listening. It SHOULD be called in the {@link Activity#onResume()}.
     *
     * @param activity Activity where will be launched the PendingIntent when the nfc reads
     *                 something.
     */
    public void enable(@NonNull Activity activity) {
        Intent intent = new Intent(activity, activity.getClass());
        PendingIntent pendingIntent = PendingIntent.getActivity(
                activity,
                Constants.RequestCode.NFC,
                intent,
                PendingIntent.FLAG_ONE_SHOT
        );
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(
                    activity,
                    pendingIntent,
                    mIntentFilters,
                    null
            );
        }
    }

    /**
     * Disables the NFC listening. It SHOULD be called in the {@link Activity#onPause()}.
     *
     * @param activity Activity where enabled the nfc listening.
     */
    public void disable(@NonNull Activity activity) {
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(activity);
        }
    }

    /**
     * Takes an Intent and tries to extract the NFC value from it.
     *
     * @param intent Intent with the NFC data read.
     * @return NFC value read from the NFC.
     */
    @Nullable
    public String getNfcValue(@NonNull Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Uri uri = intent.getData();
            if (uri != null
                    && mScheme.equals(uri.getScheme())
                    && mHost.equals(uri.getAuthority())
                    && uri.getPath() != null
                    && uri.getPath().length() > 0) {
                return uri.getPath().substring(1);
            }
        }
        return null;
    }
}
