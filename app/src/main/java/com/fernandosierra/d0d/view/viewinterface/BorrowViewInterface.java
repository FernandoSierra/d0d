package com.fernandosierra.d0d.view.viewinterface;

import android.support.annotation.UiThread;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/11/16
 */
public interface BorrowViewInterface extends BaseViewInterface {

    /**
     * Sets the views to display the information for a device to be borrow.
     */
    @UiThread
    void drawBorrowScenario();

    /**
     * Sets the views to display the information for a device to be return.
     */
    @UiThread
    void drawReturnScenario();

    /**
     * Shows an error when we tried to borrow the device, but the user did not enter a holder.
     */
    @UiThread
    void showEmptyHolderError();

    /**
     * Close the Soft-keyboard on demand.
     */
    @UiThread
    void closeKeyboard();

    /**
     * Backs in navigation.
     */
    @UiThread
    void back();
}
