package com.fernandosierra.d0d.view.viewinterface;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import com.fernandosierra.d0d.data.model.Device;

import java.util.List;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public interface DevicesViewInterface extends BaseViewInterface {

    /**
     * Updates the devices pulled from network and update the view with the devices entered as
     * parameter.
     *
     * @param devices Devices pulled from the network.
     */
    @UiThread
    void updateDevices(@NonNull List<Device> devices);

    /**
     * Shows a visual progress indicator to notify that are tasks running in background.
     */
    @UiThread
    void showProgress();

    /**
     * Hides the visual progress indicator.
     */
    @UiThread
    void hideProgress();

    /**
     * Navigates to the borrow screen and sends the device entered as parameter.
     *
     * @param device Device to be display the borrow screen.
     */
    @MainThread
    void navigateToBorrow(@NonNull Device device);
}
