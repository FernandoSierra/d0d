package com.fernandosierra.d0d.presentation;

import com.fernandosierra.d0d.data.network.D0dApi;
import com.fernandosierra.d0d.view.viewinterface.BaseViewInterface;

import rx.Scheduler;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class BasePresenter<T extends BaseViewInterface> {
    D0dApi mApi;
    Scheduler mBackground;
    Scheduler mMain;
    T mViewInterface;
    CompositeSubscription mCompositeSubscription;

    BasePresenter(D0dApi api, Scheduler background, Scheduler main) {
        mApi = api;
        mCompositeSubscription = new CompositeSubscription();
        mBackground = background;
        mMain = main;
    }

    public void setViewInterface(T viewInterface) {
        mViewInterface = viewInterface;
    }

    public void onStop() {
        mCompositeSubscription.clear();
    }

    public void onDestroy() {
        mViewInterface = null;
    }
}
