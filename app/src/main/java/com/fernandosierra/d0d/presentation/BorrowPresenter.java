package com.fernandosierra.d0d.presentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.data.network.D0dApi;
import com.fernandosierra.d0d.view.viewinterface.BorrowViewInterface;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 3/11/16
 */
public class BorrowPresenter extends BasePresenter<BorrowViewInterface> {

    public BorrowPresenter(D0dApi api, Scheduler background, Scheduler main) {
        super(api, background, main);
    }

    /**
     * Checks the device's holder attribute, if it is null, then will call
     * {@link BorrowViewInterface#drawBorrowScenario()}, in other case it will call
     * {@link BorrowViewInterface#drawReturnScenario()}.
     *
     * @param device The device that be borrowed or returned.
     */
    public void evaluateDeviceAndDraw(@NonNull Device device) {
        if (device.getHolder() == null) {
            mViewInterface.drawBorrowScenario();
        } else {
            mViewInterface.drawReturnScenario();
        }
    }

    /**
     * Triggers the process to change the device's holder name and save it as borrowed.
     * <p>
     * Note: this is an asynchronous process and when it finished calls to
     * {@link BorrowViewInterface#back()} to returns to the
     * {@link com.fernandosierra.d0d.view.DevicesActivity}.
     *
     * @param device The device that be borrowed.
     */
    public void borrowDevice(Device device) {
        mViewInterface.closeKeyboard();
        if (device.getHolder() == null || device.getHolder().isEmpty()) {
            mViewInterface.showEmptyHolderError();
        } else {
            mViewInterface.showProgressDialog();
            mCompositeSubscription.add(getBorrowDeviceSubscription(device));
        }
    }

    @NonNull
    private Subscription getBorrowDeviceSubscription(@NonNull Device device) {
        return getUpdateSubscription(device, getDeviceToUpdate(device.getHolder()));
    }

    @NonNull
    Device getDeviceToUpdate(@Nullable String holder) {
        Device device = new Device();
        device.setHolder(holder);
        return device;
    }

    private Subscription getUpdateSubscription(@NonNull Device device, Device updated) {
        Observable<Response<ResponseBody>> observable = mApi.updateDeviceByNfc(device.getNfc(),
                updated);
        return observable
                .subscribeOn(mBackground)
                .observeOn(mMain)
                .subscribe(
                        responseBody -> {
                            mViewInterface.hideProgressDialog();
                            mViewInterface.back();
                        },
                        throwable -> {
                            mViewInterface.hideProgressDialog();
                            mViewInterface.showError();
                        });
    }

    /**
     * Triggers the process to change the device's holder name and save it as returned.
     * <p>
     * Note: this is an asynchronous process and when it finished calls to
     * {@link BorrowViewInterface#back()} to returns to the
     * {@link com.fernandosierra.d0d.view.DevicesActivity}.
     *
     * @param device The device that be returned.
     */
    public void returnDevice(Device device) {
        mViewInterface.showProgressDialog();
        mCompositeSubscription.add(getReturnDeviceSubscription(device));
    }

    @NonNull
    private Subscription getReturnDeviceSubscription(@NonNull Device device) {
        return getUpdateSubscription(device, getDeviceToUpdate(null));
    }
}
