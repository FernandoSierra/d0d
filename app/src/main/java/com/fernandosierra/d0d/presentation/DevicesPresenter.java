package com.fernandosierra.d0d.presentation;

import android.support.annotation.NonNull;

import com.fernandosierra.d0d.data.model.Device;
import com.fernandosierra.d0d.data.network.D0dApi;
import com.fernandosierra.d0d.view.viewinterface.DevicesViewInterface;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class DevicesPresenter extends BasePresenter<DevicesViewInterface> {

    public DevicesPresenter(D0dApi api, Scheduler background, Scheduler main) {
        super(api, background, main);
    }

    /**
     * Triggers the process to pull the server's devices.
     * <p>
     * Note: this is an asynchronous process and returns the results in
     * {@link DevicesViewInterface#updateDevices(List)}.
     */
    public void updateDevices() {
        mViewInterface.showProgress();
        mCompositeSubscription.add(getDevices());
    }

    @NonNull
    private Subscription getDevices() {
        Observable<List<Device>> observable = mApi.getDevices();
        return observable
                .map(devices -> {
                    Collections.sort(devices);
                    return devices;
                })
                .subscribeOn(mBackground)
                .observeOn(mMain)
                .subscribe(devices -> {
                            mViewInterface.updateDevices(devices);
                            mViewInterface.hideProgress();
                        },
                        throwable -> {
                            mViewInterface.hideProgress();
                            mViewInterface.showError();
                        });
    }

    /**
     * /**
     * Triggers the process to get the device by nfc id.
     * <p>
     * Note: this is an asynchronous process and returns the result in
     * {@link DevicesViewInterface#navigateToBorrow(Device)}.
     *
     * @param nfc NFC id to be queried.
     */
    public void getDeviceByNfc(@NonNull String nfc) {
        mViewInterface.showProgressDialog();
        mCompositeSubscription.add(getDevice(nfc));
    }

    @NonNull
    private Subscription getDevice(@NonNull String nfc) {
        Observable<Device> observable = mApi.getDeviceByNfc(nfc);
        return observable
                .subscribeOn(mBackground)
                .observeOn(mMain)
                .subscribe(device -> {
                            mViewInterface.hideProgressDialog();
                            mViewInterface.navigateToBorrow(device);
                        },
                        throwable -> {
                            mViewInterface.hideProgressDialog();
                            mViewInterface.showError();
                        });
    }
}
