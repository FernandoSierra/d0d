package com.fernandosierra.d0d.data.network;

import android.support.annotation.NonNull;

import com.fernandosierra.d0d.data.model.Device;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public interface D0dApi {
    String ENDPOINT = "api/devices";
    String NFC_VALUE = "nfcValue";
    String DEVICE = ENDPOINT + "/{" + NFC_VALUE + "}";

    @GET(ENDPOINT)
    Observable<List<Device>> getDevices();

    @GET(DEVICE)
    Observable<Device> getDeviceByNfc(@NonNull @Path(NFC_VALUE) String nfc);

    @PUT(DEVICE)
    Observable<Response<ResponseBody>> updateDeviceByNfc(@NonNull @Path(NFC_VALUE) String nfc,
                                                         @NonNull @Body Device device);

    @POST(ENDPOINT)
    Observable<Device> createDevice(@NonNull @Body Device device);
}
