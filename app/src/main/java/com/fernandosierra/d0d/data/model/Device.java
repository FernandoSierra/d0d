package com.fernandosierra.d0d.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class Device implements Parcelable, Comparable<Device> {
    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };
    private static final String ID = "_id";
    private static final String NFC = "nfcvalue";
    private static final String HOLDER = "holdername";
    private static final String NAME = "devicename";
    @SerializedName(ID)
    private String mId;
    @SerializedName(NFC)
    private String mNfc;
    @SerializedName(HOLDER)
    private String mHolder;
    @SerializedName(NAME)
    private String mName;

    protected Device(Parcel in) {
        mId = in.readString();
        mNfc = in.readString();
        mHolder = in.readString();
        mName = in.readString();
    }

    public Device() {
        // Nothing
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getNfc() {
        return mNfc;
    }

    public void setNfc(String nfc) {
        mNfc = nfc;
    }

    public String getHolder() {
        return mHolder;
    }

    public void setHolder(String holder) {
        mHolder = holder;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        if (!mId.equals(device.mId)) return false;
        if (!mNfc.equals(device.mNfc)) return false;
        if (mHolder != null ? !mHolder.equals(device.mHolder) : device.mHolder != null)
            return false;
        return mName != null ? mName.equals(device.mName) : device.mName == null;

    }

    @Override
    public int hashCode() {
        int result = mId.hashCode();
        result = 31 * result + mNfc.hashCode();
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mNfc);
        dest.writeString(mHolder);
        dest.writeString(mName);
    }

    @Override
    public int compareTo(@NonNull Device another) {
        if (mHolder == null) {
            if (another.mHolder == null) {
                return mName.compareTo(another.mName);
            } else {
                return -1;
            }
        } else {
            if (another.mHolder == null) {
                return 1;
            } else {
                return mName.compareTo(another.mName);
            }
        }
    }
}
