package com.fernandosierra.d0d.util;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class Constants {
    public static final String TAG_LOG = "D0D";
    public static final String DIALOG_TAG = "device_details";

    private Constants() {
        // Nothing
    }

    public class Extra {
        public static final String DEVICE = "device";
    }

    public class RequestCode {
        public static final int NFC = 7777;
    }
}
