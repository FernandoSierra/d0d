package com.fernandosierra.d0d;

import android.app.Application;

import com.fernandosierra.d0d.injection.D0dComponent;
import com.fernandosierra.d0d.injection.D0dModule;
import com.fernandosierra.d0d.injection.DaggerD0dComponent;

/**
 * @author Jesús Fernando Sierra Pastrana
 * @version 1.0
 * @since 2/11/16.
 */
public class D0dApplication extends Application {
    protected D0dComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        D0dModule module = new D0dModule();
        module.setContext(this);
        mComponent = DaggerD0dComponent.builder()
                .d0dModule(module)
                .build();
    }

    public D0dComponent getComponent() {
        return mComponent;
    }
}
